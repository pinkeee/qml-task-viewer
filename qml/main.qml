import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: window
    width: 720 * 0.5
    height: 1440 * 0.5
    visible: true
    title: qsTr("Taskex")

    footer : TabBar {
        id: bar
        width: parent.width

        TabButton {
            text: qsTr("Usage")
            onClicked: {
                stackView.push("Useage.qml")
            }
        }
        TabButton {
            text: qsTr("Temp")
            onClicked: {
                stackView.push("Temp.qml")
            }
        }
        TabButton {
            text: qsTr("Info")
            onClicked: {
                stackView.push("Info.qml")
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "Useage.qml"
        anchors.fill: parent
    }
}
