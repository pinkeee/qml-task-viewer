import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    width: 720 * 0.5
    height: 1440 * 0.5

    title: qsTr("Temp")

    Label {
        text: qsTr("You are on Page 2.")
        anchors.centerIn: parent
    }
}
