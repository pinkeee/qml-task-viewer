import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    width: 720 * 0.5
    height: 1440 * 0.5

    title: qsTr("Info")

    Label {
        text: qsTr("You are on Page info.")
        anchors.centerIn: parent
    }
}
